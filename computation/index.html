<!doctype html>
<html lang="en">
  <head>  
  <!-- Google tag (gtag.js) -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=G-3SVS4XQ47V"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());
  
    gtag('config', 'G-3SVS4XQ47V');
  </script>
  <meta charset="utf-8">
  <meta name="author" content="Jim Hefferon" />
  <meta name="copyright" content="Contents of this HTML page and its CSS is CCSA 4.0." />
  <link rel="canonical" href="https://hefferon.net/computation/" />
  <meta property="og:locale" content="en_US" />
  <meta property="og:type" content="website" />
  <meta property="og:title" content="Free Theory of Computation text from Jim Hefferon" />
  <meta property="og:description" content="Free Theory of Comptation Text, from Jim Hefferon" />
  <meta property="og:url" content="https://hefferon.net/computation" />
  <meta property="og:site_name" content="Free math texts" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Literata">
  <link rel="stylesheet" href="../hefferon.css">
  <title>Free Theory of Computation textbook</title>
</head>

<body>
  <div id="container">
    <div id="header">
      <p>
	<i>Theory of Computation</i>
      </p>
    </div>
    <div id="content">
      <div id="innercontent">
	<img id="cover"
	     src="cover.png"
	     alt="cover of Theory of Computation by Hefferon" />
	<p>
	  <a href="https://jheffero.w3.uvm.edu/computation/book.pdf"><i>Theory of Computation</i></a> by Jim Hefferon,
	  along with its companion
	  <a href="https://jheffero.w3.uvm.edu/computation/answers.pdf">answers to exercises</a>, 
	  is a text for a one semester
	  first undergraduate Computer Science theory course.
	  It is Free.
	  Use it as the main book, as a supplement, or for independent study.
	</p>

	<h3>
	  Highlights
	</h3>
	
	<ul class="nobullet">
	  <li><span class="listitem">Standard coverage</span>
	    Definition of computation with Turing machines,
	    unsolvable problems starting with the Halting problem,
	    languages and graphs, Finite State machines,
	    and complexity including <i>P</i> vs <i>NP</i>.
            More detail is in the
	    <a href="toc.html">Table of Contents</a>.
          </li>

	  <li><span class="listitem">Liberal development</span>
	    This book 
	    encourages students to engage with the material conceptually.
            For example, we start with the definition
	    of a computable function using Turing machines and
	    thus on the first page we are tackling the subject's most
	    natural and compelling question:&nbsp;What can be done?
	    We follow that with a careful discussion of Church's Thesis.
	    The development also addresses
	    other topics that
	    can give trouble such as nondeterminism,
	    fine points of Big&nbsp;<i>O</i>,
	    and the status of the <i>P=NP</i> question.
	    This engagement helps students build a
	    robust mental framework to support the technical results.
	    That is, this text is distinguished by 
	    an exposition prompting students
	    to reflect on the ideas
	    underlying the formalities and 
	    to make connections with their other work.
	  </li>

	  <li><span class="listitem">Lively</span>
	    The presentation is stimulating without sacrificing precision.
	    There are many illustrations (more than
	    a figure per page) along with many links
	    and notes encouraging students to dive deeper.
	    A bonus of this readability is that it
	    supports instructors who incorporate
	    <a href="https://www.ams.org/publications/journals/notices/201702/rnoti-p124.pdf">active learning methods</a>, such as having students
	    read material independently.
	  </li>

	  <li><span class="listitem">Exercises</span>
	    Students learn the most while doing problems.
	    Each section ends with many exercises&nbsp;&mdash;
	    the book has more than eight hundred&nbsp;&mdash;
	    ranging from straightforward to quite challenging.
	    They include exercises that explicitly address common
	    misconceptions.
	    The answer book has fully-worked solutions to each one.
	    (In the PDF, click on an exercise number to go to the answer
	    and click
	    on the answer number to go back to the exercise.)
          </li>

	  <li><span class="listitem">Extras</span>
	    There are <a href="slides.html">beamer slides</a>
	    for classroom presentation, along with
	    <a href="https://www.youtube.com/playlist?list=PLwF3A0R8OzMpO6_9WbT1kK16akYFh3_Nt">videos</a>
	    based on those slides.
	    Also, chapters close with a number of short  
	    supplemental topics that 
            are useful for a brief digression,
	    for independent or assigned reading, or for
	    honors or small group projects.
	    There are also machine simulation programs, written in Racket.
            <!-- And there is a set of Jupyter notebooks -->
	    <!-- that allow students to work -->
	    <!-- through some of the details in code. -->
            <!-- These include -->
            <!-- <i>Simulating Turing Machines</i>, -->
	    <!-- <i>Counting with Cantor's Functions</i>, -->
	    <!-- <i>Encoding Turing Machines</i>, -->
            <!-- <i>Building a Universal Turing Machine</i>, -->
	    <!-- <i>Regular Expressions in Practice</i>, -->
            <!-- and <i>Using a SAT Solver</i>. -->

	  <li><span class="listitem">Prerequisites</span>
	    A standard course in  
	    Discrete Mathematics:&nbsp;propositional logic,
	    predicates,
	    proof methods including induction,
	    number theory such as primes and factoring, and
	    sets, functions, and relations.
	    The book has reviews for the topics of strings, functions, and
	    propositional logic.
	    It also includes sections 
	    on grammars, graph theory, and Big&nbsp;<i>O</i>
	    (this uses
	    L'H&ocirc;pital's rule and so requires derivatives),
	    which may be new for students in some majors but review for others.
	  </li>

	  <li><span class="listitem">Free</span>
	    Everything is <a href="../source.html">Freely available</a>,
	    including the
	    <a href="https://gitlab.com/jim.hefferon/toc"><span class="latex">L<sup>a</sup>T<sub>e</sub>X</span> source</a>.
	  </li>
	</ul>
	<h3 id="current_version">Current version</h3>
	<p>
	  The book is ready to be used, version 1.10.
	  I have classroom tested the material a number of times.
	  I greatly appreciate <a href="../jimhefferon.html">feedback</a>,
	  including bug reports.
	</p>
	<p>
	  The text works well in any PDF viewer.
	  A few of the figures are animated or have opacity
	  and to see these use a PDF viewer that supports those features, 
	  such as Adobe Reader.
	  In particular, my browser's built&nbsp;in PDF viewer does not
	  show some figures that do appear in my desktop's Atril or  
	  Adobe Reader.
	</p>
      </div>
    </div>
    <div id="navbar">
      <div id="innernavbar">
	<h3>
	  More on <i>Theory of Computation</i>
	</h3>
	<ul>
	  <li><a href="hardcopy.html">Get a printed copy</a>.</li>
	  <li><a href="toc.html">Table of contents</a>.</li>
	  <li><a href="teaching.html">Are you teaching with this?</a></li>
	  <li><a href="../source.html">License and source</a>.</li>
	  <li><a href="../help.html">Help out</a>.
	    Donate a little or make a report.</li>
	</ul>
	<h3>
	  You may also like ... 
	</h3>
	<ul>
	  <li><a href="/linearalgebra/"><i>Linear Algebra</i></a>,
	      a popular undergraduate text.</li>
	  <li><a href="/proofs/"><i>Introduction to Proofs</i></a>,
	      for an Inquiry-Based course.</li>
	  <li><a href="/other/">More</a>,
	      including programs and documents.</li>
	  <li><a href="/jimhefferon.html">About me</a>.</li>
	  <li><a href="/">This site's home page</a>.</li>
	</ul>
      </div>
    </div>
    <div id="footer">
      <a id="contact" href="/jimhefferon.html">Contact me</a>
    </div>
</body>
</html>
