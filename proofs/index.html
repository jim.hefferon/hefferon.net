<!doctype html>
<html lang="en">
  <head>
  <!-- Google tag (gtag.js) -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=G-3SVS4XQ47V"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());
  
    gtag('config', 'G-3SVS4XQ47V');
  </script>
  <meta charset="utf-8">
  <meta name="author" content="Jim Hefferon" />
  <meta name="copyright" content="Contents of this HTML page and its CSS is CCSA 4.0." />
  <link rel="canonical" href="https://hefferon.net/proofs/" />
  <meta property="og:locale" content="en_US" />
  <meta property="og:type" content="website" />
  <meta property="og:title" content="Introduction to Mathematical Proofs" />
  <meta property="og:description" content="Free Introduction to Proofs Text, from Jim Hefferon" />
  <meta property="og:url" content="https://hefferon.net/proofs" />
  <meta property="og:site_name" content="Free Math and Computer Science texts" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Literata">
  <link rel="stylesheet" href="../hefferon.css">
  <title>Free Introduction to Mathematical Proofs textbook</title>
</head>

<body>
  <div id="container">
    <div id="header">
      <p>
	<i>Introduction to Proofs</i>
      </p>
    </div>
    <div id="content">
      <div id="innercontent">
	<img  id="cover"
	      src="cover.png"
	      alt="cover of Introduction to Proofs by Hefferon" />
	<p>
	  <a href="https://jheffero.w3.uvm.edu/proofs/ibl-compact.pdf"><i>Introduction to Proofs: an Inquiry-based Approach</i></a> by Jim Hefferon
	  is for a Mathematics major course that is inquiry-based,
	  sometimes known as &ldquo;Moore method.&rdquo;
	  It is Free.
	</p>
	<h3>
	  Highlights
	</h3>
	<ul class="nobullet">
	  <li><span class="listitem">Inquiry-based</span>
            The text provides the material's outline,
	    a sequence of statements  
            along with a few definitions and remarks.
          </li>

	  <li><span class="listitem">Covers needed material</span>
            Number Theory up to the Fundamental Theorem of Arithmetic,
            sets to DeMorgan's Laws,
            functions to two-sided inverses,
            and
            relations to equivalences and partitions.
          </li>
	  
	  <li><span class="listitem">No prerequisite</span>
            We enroll sophomore Mathematics majors, who typically 
            have taken Linear Algebra,
            just to ensure that they have mathematical aptitude. 
          </li>
	  
	  <li><span class="listitem">Free</span>&nbsp;
            You are <a href="../source.html">free</a> to use the text 
            and free to share it.
            With the
	    <a href="https://gitlab.com/jim.hefferon/proofs"><span class="latex">L<sup>a</sup>T<sub>e</sub>X</span> source</a>,
	    you can modify it to suit your class.
          </li>
	  
	  <li><span class="listitem">Flexibility</span>
            The text comes in two presentations,
	    <a href="https://jheffero.w3.uvm.edu/proofs/ibl-compact.pdf">one of which</a>
	    is compact enough&nbsp;&mdash; seven sheets of paper&nbsp;&mdash;
            to just hand out on the first day. 
	    The
	    <a href="https://jheffero.w3.uvm.edu/proofs/ibl-maxlength.pdf">second presentation</a>
	    adds a preface and a section on infinity.
          </li> 
	  
	  <li><span class="listitem">Extras</span>
            My
            <a href="https://jheffero.w3.uvm.edu/proofs/dayone.pdf">first day's slides</a>
	    introduce students to Inquiry-Based learning.
            The
	    <a href="https://jheffero.w3.uvm.edu/proofs/logic.pdf">slides on logic</a>
	    organize the discussions that arise naturally. 
            I go through them a little per day in the first two weeks.
          </li>
	</ul>
	<h3>What is Inquiry-Based?</h3>
	<p>
          The class works through the material together,
	  so the instructor is more of a guide or mentor than a lecturer.
	  Through peer discussion, as well as learning what is right, students
	  also come to understand why the wrong things are wrong.
	  This is the best way to develop mathematical maturity.
	  The
	  <a href="https://www.ne-iblm.org/inquiry-based-learning">New England Community for Inquiry-Based Learning in Mathematics</a>
	  is a great place to get started looking for more.
	</p>
	<h3>What I do</h3>
	<p>We run a semester course, meeting three times a week.
	  I cover the first three chapters.
	</p>
	<p>
          At the first meeting, I explain that in this course we develop
	  each person&apos;s ability, as a future professional,
	  to work independently.
	  Students pledge that they will not work together outside of class
	  and will not use any resources
          such as other books or the Internet.
	  Consequently, each person arrives at
          each class having thought carefully about each exercise, on their own.
	</p>
	<p>
          Usually there are four exercises.
	  I randomly pick students to put proposed solutions on the board.
	  (I shuffle index cards.
	  The picked students negotiate among themselves over who will do
	  which one, a student picked in the prior class will not be picked
	  today, and each student is allowed twice
	  in the semester to take a pass.)
	</p>
	<p>
	  With that, the work starts.
	  The class as a whole discusses the proposals, sentence by
	  sentence and often word by word.
	  There are misconceptions to get past and ideas that the
	  class comes to see are not fruitful,
	  as well as good ideas that may initially have trouble getting heard.
	  But the discussions eventually come to an end,
	  and usually that end is correct.
	</p>
	<p>
	  I do my best not to speak, and to not even nod or frown.
	  Sometimes I must guide, for instance saying after a class has
	  decided that the proof is
	  OK,
	  &ldquo;Exhibiting the <i>n</i> = 3 and <i>n</i> = 5
	    cases is not enough to prove the
	    result for all odd numbers.&rdquo;
	</p>
	<p>
	  The discussion often takes the entire period but occasionally we
	  have a little extra time.
	  By experience I know that some problems will be a challenge and
	  I try arrange the assignments so that there is time to give
	  students a boost with these.
	  For instance, I might give an example suggesting how to show that
	  corresponding finite sets have the same cardinality.
	</p>
	<p>
	  With that, class ends with a new set of problems to work on at home,
	  and once a week with the designation of one of them to be handed in
	  at the start of the next class, to be graded.
	</p>
	<p>
	  I will close with a few comments.
	  One is that in the first two weeks, I start
	  class with fifteen minutes of working through slides that cover
	  elementary logic.
	  These provide a vocabulary and sharpen the discussions,
	  particularly about fine points such as vacuous
	  implication.
	</p>
	<p>
	  The second comment is on who is in the class.
	  We enroll sophomores who have had Calculus&nbsp;III
	  and Linear Algebra,
	  so we can expect that they have an aptitude, as well as some
	  basic scaffolding of mathematical reasoning on which to base
	  this class&apos;s development.
	  We limit to twenty students
	  because in a too-large class people can hope to hide,
	  both from being picked
	  to propose solutions and from full participation in the discussion.
	  Besides, with more than that, the marking burden discourages an
	  instructor from giving complete attention to each student&apos;s work.
	</p>
	<p>
	  Finally, about grading.
	  I count four things:&nbsp;in-class contributions,
	  the weekly hand-in problems,
	  an in-class midterm, and an in-class final.
	  In all four, I give credit both for mathematical correctness and
	  for mathematical writing.
	  I weigh the four equally, but students agree that the in-class
	  discussion is the core experience.
	  Success flows from that.
	</p>
      </div>
    </div>
    <div id="navbar">
      <div id="innernavbar">
	<h3>
	  You may also like ... 
	</h3>
	<ul>
	  <li><a href="/linearalgebra/"><i>Linear Algebra</i></a>,
	      a popular undergraduate text.</li>
	  <li><a href="/computation/"><i>Theory of Computation</i></a>,
	      for a first CS theory course.</li>
	  <li><a href="/other/">More</a>,
	      including programs and documents.</li>
	  <li><a href="/jimhefferon.html">About me</a>.</li>
	  <li><a href="/">This site's home page</a>.</li>
	</ul>
      </div>
    </div>
    <div id="footer">
      <a id="contact" href="/jimhefferon.html">Contact me</a>
    </div>
</body>
</html>
